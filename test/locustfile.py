from locust import HttpLocust, TaskSet

def test1(l):
    l.client.get("/test1.php")

def test2(l):
    l.client.get("/test1.php")

class UserBehaviour(TaskSet):
    tasks = {test1:2, test2:2}
class WebsiteUser(HttpLocust):
    task_set = UserBehaviour
    host = "http://webstack"
