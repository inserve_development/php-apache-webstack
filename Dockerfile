FROM php:5.6-apache


# Install PHP modules
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        libmysql++-dev \
        libxml2-dev \
        git \
    && docker-php-ext-install iconv mcrypt soap pdo pdo_mysql mysql mysqli mbstring \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd \
    && a2enmod rewrite

# Extend distribution standard apache conf with project specific settings
#ADD deploy/apache2.conf /tmp/apache_append.conf
#RUN cat /tmp/apache_append.conf >> /etc/apache2/apache2.conf && rm /tmp/apache_append.conf

# Add custom php.ini
#ADD deploy/php.ini /usr/local/etc/php/php.ini

# Activate built-in zend opcode cache for php5.6
RUN php -r "phpinfo(INFO_CONFIGURATION);" | grep extension_dir | awk -F '=> ' '{print $3}' > /tmp/ext_path \
    && echo "zend_extension=`cat /tmp/ext_path`/opcache.so" > /usr/local/etc/php/conf.d/opcache.ini

# Install and proivde node for assets compilation   
# verify gpg and sha256: http://nodejs.org/dist/v0.10.31/SHASUMS256.txt.asc
# gpg: aka "Timothy J Fontaine (Work) <tj.fontaine@joyent.com>"
# gpg: aka "Julien Gilli <jgilli@fastmail.fm>"
RUN gpg --keyserver pool.sks-keyservers.net --recv-keys 7937DFD2AB06298B2293C3187D33FF9D0246406D 114F43EE0176B71C7BC219DD50A3051F888C628D
ENV NODE_VERSION 0.10.38
ENV NPM_VERSION 2.11.1
RUN curl -SLO "http://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz" \
    && curl -SLO "http://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
    && gpg --verify SHASUMS256.txt.asc \
    && grep " node-v$NODE_VERSION-linux-x64.tar.gz\$" SHASUMS256.txt.asc | sha256sum -c - \
    && tar -xzf "node-v$NODE_VERSION-linux-x64.tar.gz" -C /usr/local --strip-components=1 \
    && rm "node-v$NODE_VERSION-linux-x64.tar.gz" SHASUMS256.txt.asc \
    && npm install -g npm@"$NPM_VERSION" \
    && npm cache clear

ADD mpm_prefork.conf /etc/apache2/mods-enabled/mpm_prefork.conf
ADD apache2-foreground /usr/local/bin/

ONBUILD RUN chown -R www-data:www-data /var/www
